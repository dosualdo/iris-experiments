(*
  A proof of a racy parallel increment,
  using a custom CMRA.
*)
From iris.heap_lang Require Import lib.par proofmode notation.
From iris.proofmode Require Import tactics.
From iris.base_logic.lib Require Import invariants.
(*From iris.algebra Require Import excl_auth excl agree csum.*)
(*From iris.algebra Require Import excl_auth frac_auth.*)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section racy.

Definition non_atomic_inc x := (let: "v" := !x in x <- "v" + #1)%E.

Definition racy : expr :=
  let: "x" := ref #0 in
  ( non_atomic_inc "x" ||| non_atomic_inc "x" );;
  !"x".

(* Goal: prove racy returns 1 or 2 *)

Inductive Commit : Type :=
   | CommitBot
   | PendingAuth
   | PendingFrag
   | Pending
   | Committed of Z.

Notation "●" := PendingAuth.
Notation "◯" := PendingFrag.
Notation "↯" := CommitBot (at level 20).
Notation "⤓ n" := (Committed n) (at level 20).

(*
  I think the above CMRA is equivalent to the following:

  Definition one_shotR := csumR (excl_authR unitO) (agreeR ZO).
  Notation "●Pending" := (Cinl (●E ())).
  Notation "◯Pending" := (Cinl (◯E ())).
  Definition Shot (n : Z) : one_shotR := Cinr (to_agree n).

  But it was easier to prove the lemmas I needed on a custom one,
  rather than digging all the tricks to manipulate the internals of
  the combinator-generated CAMRAs and all the typeclasses involved...
*)


Canonical Structure CommitC := leibnizO Commit.


Instance commit_op : Op Commit :=
  λ a b,
    match (a,b) with
    | (PendingAuth,PendingFrag) => Pending
    | (PendingFrag,PendingAuth) => Pending
    | (Committed n,Committed m) => if (decide (n=m)) then Committed n else CommitBot
    | (_,_) => CommitBot
    end.

Instance commit_valid : Valid Commit :=
  λ a, a ≠ CommitBot.

Instance commit_core : PCore Commit :=
  λ a, match a with
  | Committed n => Some (Committed n)
  | CommitBot => Some CommitBot
  | _ => None
  end.

Ltac simpl_commit H :=
  rewrite  /op /cmra_op /commit_op /= /valid /commit_valid /= /pcore /commit_core /= in H.
Ltac simpl_commit_goal :=
  rewrite  /op /cmra_op /commit_op /= /valid /commit_valid /= /pcore /commit_core /=.
Tactic Notation "simpl" "commit" := simpl_commit_goal.
Tactic Notation "simpl" "commit" "in" ident(H) := simpl_commit H.

Definition commit_mixin : RAMixin Commit.
Proof.
  split; try apply _; try done.
  - intros x y c E P.
    inversion E; subst.
    eauto.
  - intros a b c.
    destruct a,b,c; eauto; simpl commit;
    destruct (decide (z = z0)); try done;
    destruct (decide (z0 = z1)); subst; try done.
    destruct (decide (z0 = z1)); subst; try done.
    destruct (decide (z = z1)); subst; try done.
  - intros a b.
    simpl commit.
    destruct a,b; try done.
    destruct (decide (z0 = z));destruct (decide (z = z0)); subst; try done.
  - intros a c C.
    simpl commit in C.
    destruct a; inversion C; simpl commit; first done.
    by destruct (decide (z=z)).
  - intros a c C.
    simpl commit in C.
    destruct a;
    inversion C;
    by simpl commit.
  - intros a b c L C.
    destruct L as (a0 & B).
    inversion B; subst; clear B.
    destruct a,a0; simpl commit; try done;
    try (
      exists (↯);
      inversion C; rewrite /pcore /=;
      split; first done;
      by exists (↯)
    ).
    inversion C; subst.
    destruct (decide (z = z0)) as [->| L].
    * exists (⤓ z0).
      split; first done.
      exists (⤓ z0).
      simpl commit; try done.
      by destruct (decide (z0=z0)).
    * exists (↯).
      split; first done.
      exists (↯).
      by simpl commit.
  - intros a b.
    by destruct a,b.
Qed.

Canonical Structure commitRA := discreteR Commit commit_mixin.

Instance commitRA_cmra_discrete : CmraDiscrete commitRA.
Proof. apply discrete_cmra_discrete. Qed.

Instance committed_comm_core n: CoreId (⤓ n).
Proof. rewrite /CoreId; reflexivity. Qed.

Instance commitbot_core: CoreId (↯).
Proof. rewrite /CoreId; reflexivity. Qed.

Lemma committed_agree n m : ✓(⤓ n ⋅ ⤓ m) -> n = m.
Proof.
  intros V.
  simpl commit in V.
  by destruct (decide (n=m)).
Qed.

Instance pending_exclusive : Exclusive (Pending).
Proof.
  intros f.
  rewrite -cmra_discrete_valid_iff.
  simpl commit.
  by destruct f; simpl.
Qed.

Class commitG Σ := CommitG { commit_inG :> inG Σ commitRA }.
Definition commitΣ : gFunctors := #[GFunctor commitRA].
Instance subG_commitΣ {Σ} : subG commitΣ Σ → commitG Σ.
Proof. solve_inG. Qed.

Context `{!heapG Σ, !spawnG Σ, !commitG Σ}.

Definition N := nroot .@ "racy".

Definition invariant (x : loc) (γ1 γ2 : gname) :=
    (∃ n1 n2 : Z,
          x ↦ #(n1 + n2)
        ∗ ( (own γ1 (⤓ n1)) ∨ (⌜ n1 = 0 ⌝ ∗ own γ1 ◯) )
        ∗ ( (own γ2 (⤓ n2)) ∨ (⌜ n2 = 0 ⌝ ∗ own γ2 ◯) )
    )%I.

Lemma inv_symm_aux x γ1 γ2 : (▷ □ (invariant x γ1 γ2 ↔ invariant x γ2 γ1))%I.
Proof.
  iNext.
  iModIntro.
  iSplitL;
  iIntros "I";
    unfold invariant;
    iDestruct "I" as (n1 n2) "(X & O1 & O2)";
    iExists n2,n1;
    replace (n2 + n1) with (n1 + n2) by lia;
    eauto with iFrame.
Qed.

Lemma inv_symm x γ1 γ2 : (inv N (invariant x γ1 γ2) ⊣⊢ inv N (invariant x γ2 γ1))%I.
Proof.
  apply bi.equiv_spec.
  split;
  iIntros "I";
  iPoseProof (inv_symm_aux x γ1 γ2) as "S1";
  iPoseProof (inv_symm_aux x γ2 γ1) as "S2";
  iApply inv_iff; eauto.
Qed.


Global Instance invariant_pers x γ1 γ2 : Persistent (inv N (invariant x γ1 γ2)).
Proof. apply _. Qed.

Lemma ghost_var_alloc :
  (|==> ∃ γ, own γ ● ∗ own γ ◯)%I.
Proof.
  iMod (own_alloc (PendingAuth ⋅ PendingFrag)) as (γ) "[??]".
  - by rewrite /op /cmra_op /commit_op /=.
  - by eauto with iFrame.
Qed.

Lemma committed_dup γ m :
  own γ (⤓ m) -∗ own γ (⤓ m) ∗ own γ (⤓ m).
Proof.
  iIntros "H".
  rewrite {1}(core_id_dup (⤓ m)) own_op.
  eauto.
Qed.

Lemma ghost_var_agree γ n m :
  own γ (⤓ n) -∗ own γ (⤓ m) -∗ ⌜ n = m ⌝.
Proof.
  iApply bi.wand_intro_r.
  rewrite -own_op own_valid_r.
  iIntros "[O %]".
  apply committed_agree in H as ->.
  eauto.
Qed.

Lemma ghost_var_update γ n :
  own γ ● -∗ own γ ◯ ==∗ own γ (⤓ n).
Proof.
  iIntros "Hγ● Hγ◯".
  iMod (own_update_2 _ _ _ (⤓ n) with "Hγ● Hγ◯").
    by apply cmra_update_exclusive with (y:=⤓ n).
  done.
Qed.

Lemma ghost_invalid_auth n : ~ ✓(⤓n ⋅ ●).
Proof.
  intro H. simpl commit in H. contradiction.
Qed.

Lemma ghost_invalid_frag n : ~ ✓(⤓n ⋅ ◯).
Proof.
  intro H. simpl commit in H. contradiction.
Qed.

Lemma select_branch γ n m P:
  (own γ (⤓ n) ∗ P ∨ own γ ◯) -∗ own γ (⤓ m) -∗ own γ (⤓ m) ∗ P ∗ ⌜n=m⌝
  .
Proof.
(*  iDestruct "A1" as "[A1|[% A1]]".
    iCombine "A1" "P1●" as "T1".
    iPoseProof (own_valid_r with "T1") as "[T1 %]".
    by apply ghost_invalid_auth in H.
Qed.
*) Admitted.

Lemma thread_spec γ1 γ2 x :
  inv N (invariant x γ1 γ2) -∗
  own γ1 ● -∗
  WP (non_atomic_inc #x)
  {{ _, own γ1 (⤓ 1) ∨ (∃ n : Z, ⌜n ≠ 0⌝ ∗ own γ1 (⤓ (1 - n)) ∗ own γ2 (⤓ n)) }}.
Proof.
  iIntros "#Hinv P1●".
  unfold non_atomic_inc.
  wp_bind (!_)%E.
  iInv N as (n1 n2) ">(HX & A1 & A2)" "Close".
  wp_load.
  iDestruct "A1" as "[A1 | [-> A1] ]".
  + iCombine "A1" "P1●" as "T1".
    iPoseProof (own_valid_r with "T1") as "[T1 %]".
    by apply ghost_invalid_auth in H.
  + iAssert ((own γ2 (⤓ n2) ∨ ⌜n2 = 0⌝ ∗ own γ2 ◯) ∗ (own γ2 (⤓ n2) ∨ ⌜n2 = 0⌝))%I with "[A2]"
    as "[In Out]".
      iDestruct "A2" as "[? | [? ?]]"; eauto with iFrame.
    iMod ("Close" with "[HX A1 In]") as "_".
      iNext; unfold invariant; iExists 0; iExists n2; eauto with iFrame.
    iModIntro.
    wp_let.
    wp_op.
    replace (0 + n2 + 1) with (1 + n2) by lia.
    iInv N as (n1' n2') ">(HX & A1 & A2)" "Close".
    wp_store.
    iDestruct "A1" as "[A1|[% A1]]".
      iCombine "A1" "P1●" as "T1".
      iPoseProof (own_valid_r with "T1") as "[T1 %]".
      by apply ghost_invalid_auth in H.
    destruct (decide (n2'=0)).
    - rewrite e.
      iAssert (⌜n2=0⌝)%I as %->.
        iDestruct "Out" as "[F2|%]"; last done.
        iDestruct "A2" as "[#C2|[_ F2']]".
        by iDestruct (ghost_var_agree with "F2 C2") as "%".
        iCombine "F2" "F2'" as "T1".
        iPoseProof (own_valid_r with "T1") as "[T1 %]".
        by apply ghost_invalid_frag in H0.
      iMod (ghost_var_update _ 1 with "P1● A1") as "#G1".
      iMod ("Close" with "[HX G1 A2]") as "_".
        iNext; unfold invariant.
        iExists 1 ; iExists 0.
        iSplitL "HX"; [ done | iSplitR; eauto with iFrame ].
      eauto.
    - iDestruct "A2" as "[ #A2 | [% _] ]"; last done.
      iDestruct "Out" as "[F2|%]".
      * iDestruct (ghost_var_agree with "F2 A2") as %<-.
        iMod (ghost_var_update _ 1 with "P1● A1") as "#G1".
        iMod ("Close" with "[HX]") as "_".
         iNext; unfold invariant.
         iExists 1 ; iExists n2.
         iSplitL "HX"; [ done | iSplitL; eauto with iFrame ].
        eauto.
      * subst.
        iMod (ghost_var_update _ (1-n2') with "P1● A1") as "#G1".
        iMod ("Close" with "[HX]") as "_".
          iNext; unfold invariant.
          iExists (1-n2'),n2'.
          replace (1 - n2' + n2') with (1 + 0) by lia.
          iSplitL "HX"; [ done | iSplitL; eauto with iFrame ].
        iModIntro.
        eauto.
Qed.

Lemma racy_spec :
  {{{ True }}} racy {{{ n, RET #n; ⌜ n = 1 ∨ n = 2 ⌝ }}}.
Proof.
  iIntros (Φ) "_ Post".
  unfold racy.
  wp_alloc x as "X".
  wp_let.
  iMod ghost_var_alloc as (γ1) "[P1● P1◯]".
  iMod ghost_var_alloc as (γ2) "[P2● P2◯]".
  iMod (inv_alloc N _ (invariant x γ1 γ2) with "[P1◯ P2◯ X]") as "#Hinv".
- iNext. iExists 0. iExists 0.
  iSplitL "X"; first iFrame.
  iSplitL "P1◯"; eauto with iFrame.
- wp_apply (wp_par
    (λ _, (own γ1 (⤓ 1) ∨ ∃ n, ⌜n ≠ 0⌝ ∗ own γ1 (⤓ (1-n)) ∗ own γ2 (⤓ n))%I)
    (λ _, (own γ2 (⤓ 1) ∨ ∃ n, ⌜n ≠ 0⌝ ∗ own γ2 (⤓ (1-n)) ∗ own γ1 (⤓ n))%I)
    with "[P1●] [P2●]").
  * (* LEFT-HAND THREAD *)
    iApply ((thread_spec γ1 γ2 x) with "Hinv P1●").
  * (* RIGHT-HAND THREAD *)
    rewrite inv_symm.
    iApply ((thread_spec γ2 γ1 x) with "Hinv P2●").
  * (* After joining we get desired postcondition *)
    iIntros (v1 v2) "(T1 & T2)".
    iNext.
    wp_seq.
    iInv N as (n1' n2') ">(HX & A1 & A2)" "Close".
    wp_load.
    iDestruct "A1" as "[#A1 | [_ B1]]"; cycle 1.
      iDestruct "T1" as "[T1|T1]";
      [|iDestruct "T1" as (n) "(_ & T1 & _)"];
        iCombine "B1" "T1" as "T";
        iPoseProof (own_valid_r with "T") as "[T %]";
        by apply ghost_invalid_auth in H.
    iDestruct "A2" as "[#A2 | [_ B2]]"; cycle 1.
      iDestruct "T2" as "[T2|T2]";
      [|iDestruct "T2" as (n) "(_ & T2 & _)"];
        iCombine "B2" "T2" as "T";
        iPoseProof (own_valid_r with "T") as "[T %]";
        by apply ghost_invalid_auth in H.
    (iDestruct "T1" as "[#T1|#T1]"; [|iDestruct "T1" as (n2) "(NZ2 & T1 & T2')"]);
    (iDestruct "T2" as "[#T2|#T2]"; [|iDestruct "T2" as (n1) "(NZ1 & T2 & T1')"]);
    try iDestruct (ghost_var_agree with "A1 T1") as %-> ;
    try iDestruct (ghost_var_agree with "A2 T2") as %-> .
    + iMod ("Close" with "[HX]") as "_".
        iNext; unfold invariant.
        iExists 1,1. iSplitL "HX"; [ done | iSplitL; eauto with iFrame].
      iModIntro.
      iApply "Post".
      eauto with arith.
    + iDestruct (ghost_var_agree with "A1 T1'") as %<-.
      replace (1-1) with (0) by eauto with arith.
      replace (1+0) with (1) by eauto with arith.
      iMod ("Close" with "[HX]") as "_".
        iNext; unfold invariant.
        iExists 1,0. iSplitL "HX"; [ done | iSplitL; eauto with iFrame].
      iModIntro.
      iApply "Post".
      eauto.
    + iDestruct (ghost_var_agree with "A2 T2'") as %<-.
      replace (1-1) with (0) by eauto with arith.
      replace (1+0) with (1) by eauto with arith.
      iMod ("Close" with "[HX]") as "_".
        iNext; unfold invariant.
        iExists 0,1. iSplitL "HX"; [ done | iSplitL; eauto with iFrame].
      iModIntro.
      iApply "Post".
      eauto.
    + iDestruct (ghost_var_agree with "A2 T2'") as %<-.
      replace (1 - (1 - n1) + (1 - n1)) with (1) by lia.
      iMod ("Close" with "[HX]") as "_".
        iNext; unfold invariant.
        iExists n1,(1-n1).
        replace (n1+(1-n1)) with (1) by lia.
        iSplitL "HX"; [ done | iSplitL; eauto with iFrame].
      iModIntro.
      iApply "Post".
      eauto.
Qed.
End racy.

