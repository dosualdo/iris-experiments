(*
  Sequential Union-Find data structure.
  Inspired by a proof by Arthur Charguéraud:
  https://gitlab.inria.fr/charguer/cfml2/-/blob/master/theories/ExampleUnionFind.v

  Differences:
  - here just partial correctness;
  - slightly more flexible heap-based representation (vs fixed array);
  - `singleton` operation;
  - no axioms added.
*)

From iris.heap_lang Require Import lib.par proofmode notation.
From iris.proofmode Require Import tactics.

Implicit Type roots parents : gmap loc loc.

Ltac split_eq x y := destruct (decide (x=y)) as [<-|].

(******************************************************************)
Section Program.
(******************************************************************)

Definition singleton : expr :=
  let: "x" := ref #0 in
  "x" <- "x";;
  "x".

Definition get_root : expr :=
  rec: "get_root" "x" :=
    let: "p" := !"x" in
    if: "p" = "x" then "x" else "get_root" "p".

Definition compress : expr :=
  rec: "compress" "r" "x" :=
    if: "x" = "r"
    then #()
    else
      let: "p" := !"x" in
      "x" <- "r";;
      "compress" "r" "p".


Definition find : expr :=
  λ: "x",
  let: "r" := get_root "x" in
  compress "r" "x" ;;
  "r".

Definition union : expr :=
  λ: "x" "y",
  let: "rx" := find "x" in
  let: "ry" := find "y" in
  if: "rx" = "ry" then #()
  else "rx" <- "ry".

(******************************************************************)
End Program.
(******************************************************************)


(******************************************************************)
Section MathematicalSpecs.
(******************************************************************)


Inductive is_root_of parents : loc -> loc -> Prop :=
  | is_root : ∀ x,
      parents !! x = Some x -> is_root_of parents x x
  | is_step : ∀ x y z,
      parents !! x = Some y -> y ≠ x -> is_root_of parents y z ->
      is_root_of parents x z.


Definition roots_of parents roots :=
  (∀ x r, (roots !! x = Some r <-> is_root_of parents x r))
  /\ (∀ x p, parents !! x = Some p -> is_Some (roots !! x)).

(*
  Lemmas on mathematical parents/roots maps
*)

Lemma parents_roots_trans roots parents x y r:
  roots_of parents roots ->
  parents !! x = Some y ->
  roots !! y = Some r ->
  roots !! x = Some r.
Proof.
  intros R XY YR.
  apply R.
  apply R in YR.
  split_eq y x.
  - trivial.
  - apply (is_step _ _ y _); auto.
Qed.

Lemma parents_roots_trans2 roots parents x y r:
  roots_of parents roots ->
  parents !! x = Some y ->
  roots !! x = Some r ->
  roots !! y = Some r.
Proof.
  intros R XY YR.
  apply R.
  split_eq r x.
  - apply R in YR.
    inversion YR ; subst; rewrite H in XY; now inversion XY; subst.
  - apply R in YR.
    inversion YR; subst. contradiction.
    rewrite ->  H in XY.
    inversion  XY; subst; auto.
Qed.


Lemma in_roots_in_parents roots parents x r:
  roots_of parents roots ->
  roots !! x = Some r ->
  ∃ p, parents !! x = Some p.
Proof.
  intros R Rx. apply R in Rx. inversion Rx; eauto.
Qed.

Lemma in_parents_in_roots roots parents x p:
  roots_of parents roots ->
  parents !! x = Some p ->
  ∃ r, roots !! x = Some r.
Proof.
  intros [H1 H2] Px. by apply H2 with (p:=p).
Qed.

Lemma roots_self_loop parents (x y : loc):
  is_root_of parents x y ->
  parents !! y = Some y.
Proof.
  intro; now induction H.
Qed.

Lemma roots_are_roots roots parents x:
  roots_of parents roots ->
  parents !! x = Some x <->
  roots !! x = Some x.
Proof.
  intros.
  split.
  - intros. apply H.
    now apply is_root.
  - intro.
    apply H in H0.
    now rewrite (roots_self_loop _ x x).
Qed.

Lemma is_root_of_inj parents x r r' :
  is_root_of parents x r -> is_root_of parents x r' -> r = r'.
Proof.
  intro H.
  revert r'.
  induction H.
  - intros.
    inversion H0; subst.
    + trivial.
    + congruence.
  - intros.
    inversion H2; subst; rewrite -> H in H3; inversion H3; subst.
    + contradiction.
    + now apply IHis_root_of with (r':=r').
Qed.


Lemma overwrite_root roots parents (x p r : loc):
  roots_of parents roots ->
  roots !! x = Some r ->
  roots_of (<[x:=r]> parents) roots.
Proof.
  intros.
  (*apply H in H0.*)
  split; [intros y ry; split | ].
  - intro Ry.
    apply H in Ry.
    induction Ry as [y|z pz].
    + apply is_root.
      split_eq x y.
      * rewrite -> (roots_are_roots _ _ _ H) in H1.
        simpl_map; congruence.
      * now rewrite lookup_insert_ne.
    + split_eq x z.
      * assert (is_root_of parents pz r).
          apply H.
          apply parents_roots_trans2 with (parents:=parents) (x:=x); auto.
        pose proof (is_root_of_inj _ _ _ _ Ry H3) as ->.
        apply roots_self_loop in IHRy.
        apply is_step with (y:=r); auto.
        -- apply lookup_insert.
        -- contradict H2; subst.
           apply (roots_are_roots _ _ _ H) in H0.
           congruence.
        -- now apply is_root.
      * rewrite <- lookup_insert_ne with (i:=x) (x0:=r) in H1; auto.
        apply is_step with (y:=pz); auto.
  - intro.
    apply H.
    apply H in H0.
    induction H1; destruct (decide (x=x0)) as [<-|]; simpl_map.
    * by simplify_eq.
    * by apply is_root.
    * simplify_eq.
      replace z with y; trivial.
      apply roots_self_loop, is_root in H0.
      now apply is_root_of_inj with (x:=y) (parents:=parents).
    * now apply is_step with (y:=y).
  - destruct H.
    intros y py Py.
    destruct (decide (y=x)) as [<-|];
    [|rewrite lookup_insert_ne in Py]; eauto.
Qed.


Definition redirect ox oy (z w : loc) (m: gmap loc loc) :=
  match (ox,oy) with
  | (Some x,Some y) => if (decide (x=w)) then <[z:=y]>m else <[z:=w]>m
  | _ => ∅ (* Not very elegant but who cares *)
  end.

Definition link roots x y :=
  map_fold (redirect (roots !! x) (roots !! y)) ∅ roots.

Lemma link_at1 roots (x rx y ry z rz : loc) :
  roots !! x = Some rx ->
  roots !! y = Some ry ->
  roots !! z = Some rz ->
  link roots x y !! z = Some (if (decide (rx=rz)) then ry else rz).
Proof.
  intros X Y.
  apply (map_fold_ind (λ m1 m2,   m2 !! z = Some rz → m1 !! z = Some (if decide (rx = rz) then ry else rz))).
- naive_solver.
- intros; rewrite X Y; unfold redirect.
  destruct (decide (rx=x0)) as [<-|];
  destruct (decide (z=i)) as [<-|];
  case_decide; simpl_map; congruence.
Qed.

Lemma link_at2 roots (x rx y ry : loc) :
  roots !! x = Some rx ->
  roots !! y = Some ry ->
  ∀ z r, link roots x y !! z = Some r ->
    ∃rz, roots !! z = Some rz /\ r = (if (decide (rx=rz)) then ry else rz).
Proof.
  intros X Y.
  apply (map_fold_ind (λ m1 m2, ∀ z r, m1 !! z = Some r -> ∃rz, m2 !! z = Some rz /\ r = (if (decide (rx=rz)) then ry else rz))).
- naive_solver.
- intros; rewrite X Y in H1; unfold redirect in H1.
  destruct (decide (z=i)) as [<-|]; case_decide; simpl_map; auto;
  [exists x0|exists r0]; case_decide; split; congruence.
Qed.

Lemma link_dichotomy parents roots (x rx y ry z r:loc):
  roots_of parents roots ->
  roots !! x = Some rx -> roots !! y = Some ry ->
  link roots x y !! z = Some r <->
  ( ( roots !! z ≠ Some rx /\ roots !! z = Some r ) \/
    ( roots !! z = Some rx /\ roots !! y = Some r ) ).
Proof.
  intros H X Y.
  split.
  - intro L.
    apply (link_at2 _ _ _ _ _ X Y) in L as [rz [Z R]].
    case_decide; [ right | left ]; split; congruence.
  - intros [[A Z]|[Z D]];
    pose proof (link_at1 _ _ _ _ _ _ _ X Y Z) as L;
    case_decide; congruence.
Qed.

Lemma link_id roots r x y:
  roots !! x = Some r ->
  roots !! y = Some r ->
  link roots x y = roots.
Proof.
  intros.
  apply map_fold_ind; trivial.
  intros a b m ? _ ->.
  rewrite H H0; unfold redirect.
  now split_eq r b.
Qed.

Lemma paths_after_link parents roots x y rx ry r :
  roots_of parents roots ->
  roots !! x = Some rx ->
  roots !! y = Some ry ->
  rx ≠ ry ->
  is_root_of (<[rx:=r]> parents) y ry.
Proof.
  intros.
  apply H in H0.
  apply H in H1.
  induction H1 as [y|y py].
  - rewrite <- (lookup_insert_ne _ rx y r) in H1; last done.
    by apply is_root.
  - apply is_step with (y:=py); auto.
    split_eq rx y.
    + apply roots_self_loop in H0; congruence.
    + by rewrite lookup_insert_ne.
Qed.

Lemma paths_after_link2 parents roots x y rx ry :
  roots_of parents roots ->
  roots !! x = Some rx ->
  roots !! y = Some ry ->
  rx ≠ ry ->
  is_root_of (<[rx:=ry]> parents) x ry.
Proof.
  intros.
  apply H in H0.
  apply H in H1.
  induction H0 as [x|x px].
  - apply is_step with (y:=ry); auto.
    by rewrite lookup_insert.
    apply roots_self_loop in H1.
    apply is_root.
    by rewrite lookup_insert_ne.
  - apply IHis_root_of in H2.
    apply is_step with (y:=px); auto.
    rewrite lookup_insert_ne; auto.
    contradict H3.
    apply roots_self_loop in H4.
    congruence.
Qed.

Lemma path_preservation1 parents x y z rx ry :
  rx ≠ ry ->
  is_root_of parents x rx ->
  is_root_of parents y ry ->
  is_root_of (<[rx:=ry]>parents) z ry ->
  (is_root_of parents z ry \/ is_root_of parents z rx).
Proof.
  intros.
  induction H2 as [z|z pz rz].
  - split_eq rx z; simpl_map; simplify_eq.
    left; by apply is_root.
  - destruct IHis_root_of as [IH|IH]; split_eq rx z.
    * right. apply roots_self_loop in H0. by apply is_root.
    * left. simpl_map; apply is_step with (y:=pz); auto.
    * right. apply roots_self_loop in H0. by apply is_root.
    * right. simpl_map; apply is_step with (y:=pz); auto.
Qed.

Lemma path_preservation2 parents x y z rx ry rz :
  rx ≠ ry -> rz ≠ ry ->
  is_root_of parents x rx ->
  is_root_of parents y ry ->
  is_root_of (<[rx:=ry]>parents) z rz ->
  (is_root_of parents z rz /\ rz ≠ rx).
Proof.
  intros.
  assert (rx≠rz).
    apply roots_self_loop in H3.
    split_eq rx rz; simpl_map; simplify_eq; auto.
  assert (∀w rw, is_root_of (<[rx:=ry]>parents) w rw -> rw = rz -> w≠rx).
  {
    intros w rw I.
    induction I.
  - intros ->. contradict H4; simplify_eq; simpl_map; simplify_eq.
  - intros ->. specialize (IHI ltac:(reflexivity)).
    contradict H6; subst.
    simpl_map; simplify_eq.
    apply roots_self_loop in H2.
    rewrite <- (lookup_insert_ne _ rx y0 y0) in H2; trivial.
    apply is_root in H2.
    contradict H0.
    apply (is_root_of_inj (<[rx:=y0]> parents) y0); auto.
   } idtac. (* this is just due to my text editor's Coq plugin bug *)
  pose proof H3 as ZX%H5; trivial.
  split; auto.
  induction H3 as [z|z pz rz]; simpl_map.
  - by apply is_root.
  - pose proof H7 as H8%H5; trivial.
    specialize (IHis_root_of H0 H4 H5 H8).
    apply is_step with (y:=pz); trivial.
Qed.

Lemma roots_after_link parents roots x y rx ry :
  roots_of parents roots ->
  roots !! x = Some rx ->
  roots !! y = Some ry ->
  rx ≠ ry ->
  roots_of (<[rx:=ry]> parents) (link roots x y).
Proof.
  intros.
  split; [intros z rz; split | ].
  - intros.
    apply (link_dichotomy _ _ _ _ _ _ _ _ H H0 H1) in H3.
    destruct H3 as [[RX K]|[RX K]].
      apply (paths_after_link _ roots x); auto.
      congruence.
      assert (ry=rz) as -> by congruence.
      by apply (paths_after_link2 _ roots z y).
  - intro.
    apply (link_dichotomy _ _ _ _ _ _ _ _ H H0 H1).
    split_eq rz ry; apply H in H0; pose proof H1 as Y%H.
    + apply (path_preservation1 _ x y z rx) in Y as [A%H|B%H]; trivial;
      [left | right]; split; congruence.
    + apply (path_preservation2 _ x y z rx ry rz) in H3 as [A%H B]; trivial.
      left; split; congruence.
  - intros.
    assert (is_Some (roots !! x0)).
      split_eq x0 rx.
        apply H,roots_self_loop,is_root,H in H0; eauto.
        simpl_map; apply (in_parents_in_roots _ parents _ p); auto.
    revert H4; generalize x0.
    apply (map_fold_ind (λ m1 m2, ∀z, is_Some (m2 !! z) → is_Some(m1 !! z))).
    + by simpl_map.
    + intros.
      rewrite H0 H1; unfold redirect.
      case_decide;
      ( split_eq z i; simpl_map; simplify_eq;
      [by eexists | by apply H5]).
Qed.

(******************************************************************)
End MathematicalSpecs.
(******************************************************************)






(******************************************************************)
Section ProgramSpecs.
(******************************************************************)

Context `{!heapG Σ}.

Definition UF roots : iProp Σ :=
  (∃ parents,
    ([∗ map] x ↦ y ∈ parents, x ↦ #(y : loc)) ∗ ⌜roots_of parents roots⌝)%I.


Definition EL roots x : iProp Σ := (⌜∃r, roots !! x = Some r⌝)%I.
(*is_Some (roots !! x)*)
Definition REP roots x r : iProp Σ := (⌜roots !! x = Some r⌝)%I.


Lemma modify_parent parents (x y z:loc) :
  parents !! x = Some y ->
  ⊢ (([∗ map] a ↦ b ∈ parents, a ↦ #(b : loc)) -∗
      x ↦ #y ∗
      (x ↦ #z -∗ ([∗ map] a ↦ b ∈ <[x:=z]>parents, a ↦ #(b : loc))))%I.
Proof.
  iIntros (H) "S".
  iDestruct (big_sepM_insert_acc with "S") as "[X C]";
  eauto with iFrame.
Qed.

Lemma read_cell parents (x y : loc) :
  parents !! x = Some y ->
  ⊢ (([∗ map] a ↦ b ∈ parents, a ↦ #(b : loc)) -∗
    x ↦ #y ∗
    (x ↦ #y -∗ ([∗ map] a ↦ b ∈ parents, a ↦ #(b : loc))))%I.
Proof.
  iIntros (H) "S".
  iDestruct (big_sepM_lookup_acc with "S") as "A";
  eauto with iFrame.
Qed.


Definition Fresh roots r : iProp Σ :=
  (*(⌜roots !! r = None⌝)%I.*)
  (∀x, EL roots x -∗ ⌜x≠r⌝)%I.

Lemma fresh_none roots r :
  Fresh roots r -∗ ⌜roots !! r = None⌝.
Proof.
  iPureIntro.
  intros.
  case_eq (roots !! r); try done.
    intros.
    pose proof (H r) as H.
    exfalso.
    apply H.
    by exists l.
    done.
Qed.

Lemma no_orphans parents roots x px:
  roots_of parents roots ->
  parents !! x = Some px ->
  is_Some (parents !! px).
Proof.
  intros [H1 H2] H3.
  pose proof H3 as H4.
  apply H2 in H3 as [rx Rx].
  apply H1 in Rx.
  generalize dependent px.
  induction Rx.
  - intros. rewrite H in H4. inversion H4; subst; eauto.
  - intros. apply H1 in Rx.
    apply (in_roots_in_parents _ parents) in Rx as [py Py]; last done.
    rewrite H in H4. inversion H4; subst; eauto.
Qed.

Lemma irrelevant_singletons parents roots r x z:
  roots_of parents roots ->
  parents !! r = None ->
  x ≠ r ->
  is_root_of (<[r:=r]>parents) x z ->
  is_root_of parents x z.
Proof.
  intros.
  induction H2.
  - rewrite lookup_insert_ne in H2; last done.
    by apply is_root.
  - rewrite lookup_insert_ne in H2; last done.
    pose proof H2 as HH.
    apply (no_orphans _ roots) in H2 as [py Py]; last done.
    split_eq y r.
    * congruence.
    * apply is_step with (y:=y); auto.
Qed.

Lemma triple_singleton : forall roots,
  {{{ UF roots }}}
    singleton
  {{{ (r:loc), RET #r; Fresh roots r ∗ UF (<[r:=r]>roots)  }}}.
Proof.
  iIntros (? ?) "UF POST".
  iDestruct "UF" as (parents) "[S %]".
  unfold singleton.
  wp_alloc r as "R".
  wp_let.
  wp_store.
  iApply "POST".
  iAssert (Fresh roots r) as %F.
    iIntros (x) "%".
    destruct H0 as [rx Rx].
    apply in_roots_in_parents with (parents:=parents) in Rx as [px Px]; last done.
    iDestruct (read_cell _ _ _ Px with "S") as "[X _]".
    iApply (mapsto_mapsto_ne x r 1 1 with "[X]"); auto.
  assert (parents !! r = None) as PNone.
    case_eq (parents !! r); auto; intros.
    apply (in_parents_in_roots roots) in H0; last done.
    by apply F in H0.
  iSplit.
  - done.
  - iExists (<[r:=r]>parents).
    iSplit.
    + iApply big_sepM_insert;
      auto with iFrame.
    + iPureIntro.
      split; [intros x rx; split | ].
      * intro.
        split_eq x r.
        -- rewrite lookup_insert in H0.
           inversion H0; subst.
           apply is_root.
           apply lookup_insert.
        -- rewrite lookup_insert_ne in H0; last done.
           apply H in H0.
           induction H0.
             apply is_root;
             by rewrite lookup_insert_ne.
           apply is_step with (y:=y); auto.
           by rewrite lookup_insert_ne.
           apply IHis_root_of.
           apply H in H2.
           apply in_roots_in_parents with (parents:=parents) in H2 as [py Y]; last done.
           congruence.
       * intro.
         split_eq x r.
         -- rewrite lookup_insert.
            inversion H0; subst; auto.
            rewrite lookup_insert in H1; congruence.
         -- inversion H0; subst.
            ++ rewrite lookup_insert_ne in H1; last done.
               apply is_root in H1.
               apply H in H1.
               by rewrite lookup_insert_ne.
            ++ rewrite lookup_insert_ne in H1; last done.
               rewrite lookup_insert_ne; last done.
               pose proof H1 as Px.
               apply (no_orphans _ roots) in H1 as [py PY]; last done.
               apply (irrelevant_singletons _ roots) in H3; auto; last congruence.
               apply H, is_step with (y:=y); auto.
       * intros.
         split_eq x r.
         -- exists x; apply lookup_insert.
         -- rewrite lookup_insert_ne in H0; last done.
            apply H in H0 as [rx ?].
            exists rx. by rewrite lookup_insert_ne.
Qed.

Lemma triple_get_root : forall roots (x:loc),
  {{{ UF roots ∗ EL roots x }}}
    (get_root #x)%E
  {{{ (r:loc), RET #r;  UF roots ∗ REP roots x r  }}}.
Proof.
  iIntros (? ? ?) "[UF EL] POST".
  wp_pures.
  iLöb as "IH" forall (x).
  iDestruct "EL" as "#EL"; iPoseProof "EL" as (rx) "%".
  iDestruct "UF" as (parents) "[S %]".
  assert (∃p, parents !! x = Some p) as [p P].
    eapply in_roots_in_parents; eauto.
  iDestruct (read_cell _ _ _ P with "S") as "[X C]".
  wp_load; wp_pures.
  iPoseProof ("C" with "X") as "S".
  case_bool_decide; simplify_eq/=; wp_pures.
- iClear "IH".
  iApply "POST".
  iSplit. iExists parents. eauto with iFrame.
  iPureIntro.
  by apply roots_are_roots with (parents:=parents).
- iAssert (UF roots) with "[S]" as "B".
    iExists parents; iSplit; trivial.
  assert (∃ rp, roots !! p = Some rp).
    exists rx.
    eapply parents_roots_trans2; eauto.
  iApply ("IH" $! p with "[B]");
  eauto with iFrame.
  iIntros (rr) "[U %]".
  iApply "POST".
  iFrame.
  iPureIntro.
  apply (parents_roots_trans roots parents _ p _); trivial.
Qed.


Lemma triple_compress : forall roots (r x:loc),
  {{{ (UF roots) ∗ ⌜roots !! x = Some r⌝ }}}
  (compress #r #x)%E
  {{{ RET #(); UF roots }}}.
Proof.
  iIntros (? ? ? ?) "[UF %] POST".
  wp_pures.
  iRevert (x H).
  iLöb as "IH".
  iIntros (x H).
  case_bool_decide; simplify_eq/=; wp_pures.
  - iApply "POST"; iFrame.
  - iDestruct "UF" as (parents) "[S %]".
    assert (∃p, parents !! x = Some p) as [p Px].
      eapply in_roots_in_parents; eauto.
    iDestruct (modify_parent _ _ _ _ Px with "S") as "[X C]".
    wp_load.
    wp_pures.
    wp_store.
    wp_pures.
    iPoseProof ("C" with "X") as "C".
    eapply parents_roots_trans2 with (roots:=roots) (parents:=parents) in Px; eauto.
    iAssert (UF roots) with "[C]" as "B".
      iExists (<[x:=r]> parents).
      iSplit.
        iFrame.
        iPureIntro. now apply overwrite_root.
    iApply ("IH" with "B POST").
    eauto with iFrame.
Qed.


Lemma triple_find : forall roots (x:loc),
  {{{ UF roots ∗ EL roots x }}}
    (find #x)%E
  {{{ (r:loc), RET #r;  UF roots ∗ REP roots x r  }}}.
Proof.
  iIntros (? ? ?) "PRE POST".
  (*iPoseProof (roots !! x = Some r)*)
  unfold find.
  wp_let; fold get_root compress.
  wp_bind (get_root _).
  iApply (triple_get_root with "PRE").
  iNext.
  iIntros (r) "[PRE #EL]".
  wp_let; fold compress.
  wp_bind (compress _ _).
  iCombine "PRE" "EL" as "PRE".
  iApply (triple_compress with "PRE").
  iNext.
  iIntros "UF".
  wp_pures.
  iApply "POST".
  eauto with iFrame.
Qed.


Lemma triple_union : forall roots (x y:loc),
  {{{ UF roots ∗ EL roots x ∗ EL roots y }}}
    (union #x #y)
  {{{ RET #(); UF (link roots x y) }}}.
Proof.
  iIntros (? ? ? ?) "(UF & #Ex & #Ey) POST".
  (*iDestruct "Ex" as (rx) "#Rx";*)
  (*iDestruct "Ey" as (ry) "#Ry".*)
  unfold union.
  wp_let; fold find.
  wp_let; fold get_root compress find.
  wp_bind (find _).
  iCombine "UF" "Ex" as "PRE".
  iApply (triple_find roots with "PRE").
  iNext.
  iIntros (rx) "[UF #Rx]".
  wp_let; fold get_root compress find.
  wp_bind (find _).
  iCombine "UF" "Ey" as "PRE".
  iApply (triple_find roots with "PRE").
  iNext.
  iIntros (ry) "[UF #Ry]".
  iPoseProof "Rx" as "%";
  iPoseProof "Ry" as "%".
  wp_pures.
  case_bool_decide; simplify_eq/=; wp_pures.
  - iApply "POST". now rewrite -> (link_id _ ry).
  - iDestruct "UF" as (parents) "[S %]".
    iDestruct ((modify_parent _ rx rx ry) with "S") as "[X C]".
      eapply roots_self_loop.
      by apply H2.
    wp_store.
    iApply "POST".
    iDestruct ("C" with "X") as "S".
    iExists _; iFrame.
    iPureIntro.
    assert (rx ≠ ry).
      intro; subst; contradiction. (*There must be an easier way*)
    by apply roots_after_link.
Qed.


(******************************************************************)
End ProgramSpecs.
(******************************************************************)